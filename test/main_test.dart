import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:teach_on_mars_techtest/models/models.dart';
import 'package:teach_on_mars_techtest/presentation/presentation.dart';
import 'package:teach_on_mars_techtest/repos/repos.dart';

import 'data/data.dart';

class MockRepository extends Mock implements FeedRepository {}

void main() {
  late MockRepository mockRepository;
  final posts = <IPost>[
    PicturePost.fromJson(data[0]),
    ArticlePost.fromJson(data[1])
  ];

  setUp(() {
    mockRepository = MockRepository();
  });

  void arrangeRepositoryReturnPosts() {
    when(mockRepository.getFeed).thenAnswer((_) async => posts);
  }

  void arrangeRepositoryReturnError() {
    when(mockRepository.getFeed).thenThrow('error');
  }

  blocTest<HomeBloc, HomeState>(
    'Fetching data success',
    build: () {
      arrangeRepositoryReturnPosts();
      return HomeBloc(mockRepository);
    },
    act: (bloc) => bloc.add(LoadHomeEvent()),
    expect: () => [HomeLoadingState(), HomeLoadedState(posts)],
  );

  blocTest<HomeBloc, HomeState>(
    'Fetching data error',
    build: () {
      arrangeRepositoryReturnError();
      return HomeBloc(mockRepository);
    },
    act: (bloc) => bloc.add(LoadHomeEvent()),
    expect: () => [HomeLoadingState(), HomeErrorState('error')],
  );
}
