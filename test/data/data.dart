final data = [
  {
    'id': '643804bd56a48',
    'type': 2,
    'title': 'Title',
    'picture': {
      'width': 800,
      'height': 800,
      'url': 'https://picsum.photos/800/800'
    },
    'author': 'Author'
  },
  {
    'id': '643804bd56a3e',
    'type': 1,
    'title': 'Title',
    'excerpt': 'Excerpt',
    'text': 'Text'
  }
];
