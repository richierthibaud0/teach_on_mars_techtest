import 'package:teach_on_mars_techtest/app/app.dart';
import 'package:teach_on_mars_techtest/bootstrap.dart';

void main() {
  bootstrap(() => const App());
}
