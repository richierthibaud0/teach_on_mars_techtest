import 'dart:convert';
import 'package:http/http.dart';
import 'package:teach_on_mars_techtest/models/models.dart';

class FeedRepository {
  // String api = 'https://interview-dev.teachonmars.com/interview-api.php';
  Client client = Client();

  Future<List<IPost>> getFeed() async {
    final uri = Uri.https('interview-dev.teachonmars.com', 'interview-api.php');
    final response = await client.get(uri);
    if (response.statusCode == 200) {
      final data = <IPost>[];
      final json =
          List<Map<String, dynamic>>.from(jsonDecode(response.body) as List);

      for (final element in json) {
        if (element.containsKey('type') == true) {
          switch (element['type']) {
            case 1:
              data.add(ArticlePost.fromJson(element));
              break;
            case 2:
              data.add(PicturePost.fromJson(element));
              break;
            default:
              break;
          }
        }
      }
      return data;
    } else {
      throw Exception(response.reasonPhrase);
    }
  }
}
