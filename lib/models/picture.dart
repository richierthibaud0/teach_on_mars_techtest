import 'package:freezed_annotation/freezed_annotation.dart';

part '../generated/models/picture.freezed.dart';
part '../generated/models/picture.g.dart';

@freezed
class Picture with _$Picture {
  const factory Picture({
    required double width,
    required double height,
    required String url,
  }) = _Picture;

  factory Picture.fromJson(Map<String, Object?> json) =>
      _$PictureFromJson(json);
}
