import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:teach_on_mars_techtest/models/models.dart';

part '../generated/models/article_post.freezed.dart';
part '../generated/models/article_post.g.dart';

@freezed
class ArticlePost with _$ArticlePost, IPost {
  const factory ArticlePost({
    required String id,
    required String title,
    required int type,
    required String excerpt,
    required String text,
  }) = _ArticlePost;

  factory ArticlePost.fromJson(Map<String, Object?> json) =>
      _$ArticlePostFromJson(json);
}
