import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:teach_on_mars_techtest/models/models.dart';

part '../generated/models/picture_post.freezed.dart';
part '../generated/models/picture_post.g.dart';

@freezed
class PicturePost with _$PicturePost, IPost {
  const factory PicturePost({
    required String id,
    required int type,
    required String title,
    required String author,
    required Picture picture,
  }) = _PicturePost;

  factory PicturePost.fromJson(Map<String, Object?> json) =>
      _$PicturePostFromJson(json);
}
