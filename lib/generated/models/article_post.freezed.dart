// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of '../../models/article_post.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ArticlePost _$ArticlePostFromJson(Map<String, dynamic> json) {
  return _ArticlePost.fromJson(json);
}

/// @nodoc
mixin _$ArticlePost {
  String get id => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  int get type => throw _privateConstructorUsedError;
  String get excerpt => throw _privateConstructorUsedError;
  String get text => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ArticlePostCopyWith<ArticlePost> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ArticlePostCopyWith<$Res> {
  factory $ArticlePostCopyWith(
          ArticlePost value, $Res Function(ArticlePost) then) =
      _$ArticlePostCopyWithImpl<$Res, ArticlePost>;
  @useResult
  $Res call({String id, String title, int type, String excerpt, String text});
}

/// @nodoc
class _$ArticlePostCopyWithImpl<$Res, $Val extends ArticlePost>
    implements $ArticlePostCopyWith<$Res> {
  _$ArticlePostCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? title = null,
    Object? type = null,
    Object? excerpt = null,
    Object? text = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as int,
      excerpt: null == excerpt
          ? _value.excerpt
          : excerpt // ignore: cast_nullable_to_non_nullable
              as String,
      text: null == text
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ArticlePostCopyWith<$Res>
    implements $ArticlePostCopyWith<$Res> {
  factory _$$_ArticlePostCopyWith(
          _$_ArticlePost value, $Res Function(_$_ArticlePost) then) =
      __$$_ArticlePostCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String id, String title, int type, String excerpt, String text});
}

/// @nodoc
class __$$_ArticlePostCopyWithImpl<$Res>
    extends _$ArticlePostCopyWithImpl<$Res, _$_ArticlePost>
    implements _$$_ArticlePostCopyWith<$Res> {
  __$$_ArticlePostCopyWithImpl(
      _$_ArticlePost _value, $Res Function(_$_ArticlePost) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? title = null,
    Object? type = null,
    Object? excerpt = null,
    Object? text = null,
  }) {
    return _then(_$_ArticlePost(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as int,
      excerpt: null == excerpt
          ? _value.excerpt
          : excerpt // ignore: cast_nullable_to_non_nullable
              as String,
      text: null == text
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ArticlePost implements _ArticlePost {
  const _$_ArticlePost(
      {required this.id,
      required this.title,
      required this.type,
      required this.excerpt,
      required this.text});

  factory _$_ArticlePost.fromJson(Map<String, dynamic> json) =>
      _$$_ArticlePostFromJson(json);

  @override
  final String id;
  @override
  final String title;
  @override
  final int type;
  @override
  final String excerpt;
  @override
  final String text;

  @override
  String toString() {
    return 'ArticlePost(id: $id, title: $title, type: $type, excerpt: $excerpt, text: $text)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ArticlePost &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.type, type) || other.type == type) &&
            (identical(other.excerpt, excerpt) || other.excerpt == excerpt) &&
            (identical(other.text, text) || other.text == text));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, title, type, excerpt, text);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ArticlePostCopyWith<_$_ArticlePost> get copyWith =>
      __$$_ArticlePostCopyWithImpl<_$_ArticlePost>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ArticlePostToJson(
      this,
    );
  }
}

abstract class _ArticlePost implements ArticlePost {
  const factory _ArticlePost(
      {required final String id,
      required final String title,
      required final int type,
      required final String excerpt,
      required final String text}) = _$_ArticlePost;

  factory _ArticlePost.fromJson(Map<String, dynamic> json) =
      _$_ArticlePost.fromJson;

  @override
  String get id;
  @override
  String get title;
  @override
  int get type;
  @override
  String get excerpt;
  @override
  String get text;
  @override
  @JsonKey(ignore: true)
  _$$_ArticlePostCopyWith<_$_ArticlePost> get copyWith =>
      throw _privateConstructorUsedError;
}
