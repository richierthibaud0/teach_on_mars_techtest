// GENERATED CODE - DO NOT MODIFY BY HAND

part of '../../models/article_post.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ArticlePost _$$_ArticlePostFromJson(Map<String, dynamic> json) =>
    _$_ArticlePost(
      id: json['id'] as String,
      title: json['title'] as String,
      type: json['type'] as int,
      excerpt: json['excerpt'] as String,
      text: json['text'] as String,
    );

Map<String, dynamic> _$$_ArticlePostToJson(_$_ArticlePost instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'type': instance.type,
      'excerpt': instance.excerpt,
      'text': instance.text,
    };
