// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of '../../models/picture_post.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PicturePost _$PicturePostFromJson(Map<String, dynamic> json) {
  return _PicturePost.fromJson(json);
}

/// @nodoc
mixin _$PicturePost {
  String get id => throw _privateConstructorUsedError;
  int get type => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  String get author => throw _privateConstructorUsedError;
  Picture get picture => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PicturePostCopyWith<PicturePost> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PicturePostCopyWith<$Res> {
  factory $PicturePostCopyWith(
          PicturePost value, $Res Function(PicturePost) then) =
      _$PicturePostCopyWithImpl<$Res, PicturePost>;
  @useResult
  $Res call(
      {String id, int type, String title, String author, Picture picture});

  $PictureCopyWith<$Res> get picture;
}

/// @nodoc
class _$PicturePostCopyWithImpl<$Res, $Val extends PicturePost>
    implements $PicturePostCopyWith<$Res> {
  _$PicturePostCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? type = null,
    Object? title = null,
    Object? author = null,
    Object? picture = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as int,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      author: null == author
          ? _value.author
          : author // ignore: cast_nullable_to_non_nullable
              as String,
      picture: null == picture
          ? _value.picture
          : picture // ignore: cast_nullable_to_non_nullable
              as Picture,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $PictureCopyWith<$Res> get picture {
    return $PictureCopyWith<$Res>(_value.picture, (value) {
      return _then(_value.copyWith(picture: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_PicturePostCopyWith<$Res>
    implements $PicturePostCopyWith<$Res> {
  factory _$$_PicturePostCopyWith(
          _$_PicturePost value, $Res Function(_$_PicturePost) then) =
      __$$_PicturePostCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id, int type, String title, String author, Picture picture});

  @override
  $PictureCopyWith<$Res> get picture;
}

/// @nodoc
class __$$_PicturePostCopyWithImpl<$Res>
    extends _$PicturePostCopyWithImpl<$Res, _$_PicturePost>
    implements _$$_PicturePostCopyWith<$Res> {
  __$$_PicturePostCopyWithImpl(
      _$_PicturePost _value, $Res Function(_$_PicturePost) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? type = null,
    Object? title = null,
    Object? author = null,
    Object? picture = null,
  }) {
    return _then(_$_PicturePost(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as int,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      author: null == author
          ? _value.author
          : author // ignore: cast_nullable_to_non_nullable
              as String,
      picture: null == picture
          ? _value.picture
          : picture // ignore: cast_nullable_to_non_nullable
              as Picture,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PicturePost implements _PicturePost {
  const _$_PicturePost(
      {required this.id,
      required this.type,
      required this.title,
      required this.author,
      required this.picture});

  factory _$_PicturePost.fromJson(Map<String, dynamic> json) =>
      _$$_PicturePostFromJson(json);

  @override
  final String id;
  @override
  final int type;
  @override
  final String title;
  @override
  final String author;
  @override
  final Picture picture;

  @override
  String toString() {
    return 'PicturePost(id: $id, type: $type, title: $title, author: $author, picture: $picture)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PicturePost &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.type, type) || other.type == type) &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.author, author) || other.author == author) &&
            (identical(other.picture, picture) || other.picture == picture));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, id, type, title, author, picture);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PicturePostCopyWith<_$_PicturePost> get copyWith =>
      __$$_PicturePostCopyWithImpl<_$_PicturePost>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PicturePostToJson(
      this,
    );
  }
}

abstract class _PicturePost implements PicturePost {
  const factory _PicturePost(
      {required final String id,
      required final int type,
      required final String title,
      required final String author,
      required final Picture picture}) = _$_PicturePost;

  factory _PicturePost.fromJson(Map<String, dynamic> json) =
      _$_PicturePost.fromJson;

  @override
  String get id;
  @override
  int get type;
  @override
  String get title;
  @override
  String get author;
  @override
  Picture get picture;
  @override
  @JsonKey(ignore: true)
  _$$_PicturePostCopyWith<_$_PicturePost> get copyWith =>
      throw _privateConstructorUsedError;
}
