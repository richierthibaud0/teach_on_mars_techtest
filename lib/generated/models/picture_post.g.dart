// GENERATED CODE - DO NOT MODIFY BY HAND

part of '../../models/picture_post.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PicturePost _$$_PicturePostFromJson(Map<String, dynamic> json) =>
    _$_PicturePost(
      id: json['id'] as String,
      type: json['type'] as int,
      title: json['title'] as String,
      author: json['author'] as String,
      picture: Picture.fromJson(json['picture'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_PicturePostToJson(_$_PicturePost instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'title': instance.title,
      'author': instance.author,
      'picture': instance.picture,
    };
