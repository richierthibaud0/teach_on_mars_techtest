import 'package:bloc/bloc.dart';
import 'package:teach_on_mars_techtest/app/app.dart';

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  ThemeBloc() : super(ThemeState(isDarkTheme: true)) {
    on<ChangeTheme>(
      (event, emit) {
        if (state.isDarkTheme) {
          emit(state.copyWith(isDarkTheme: false));
        } else {
          emit(state.copyWith(isDarkTheme: true));
        }
      },
    );
  }
}
