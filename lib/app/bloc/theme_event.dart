import 'package:flutter/widgets.dart';

@immutable
abstract class ThemeEvent {}

class ChangeTheme extends ThemeEvent {}
