class ThemeState {
  ThemeState({required this.isDarkTheme});
  final bool isDarkTheme;

  ThemeState copyWith({bool? isDarkTheme}) {
    return ThemeState(isDarkTheme: isDarkTheme ?? this.isDarkTheme);
  }
}
