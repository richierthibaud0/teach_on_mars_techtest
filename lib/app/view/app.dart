import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teach_on_mars_techtest/app/app.dart';
import 'package:teach_on_mars_techtest/l10n/l10n.dart';

import 'package:teach_on_mars_techtest/presentation/presentation.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ThemeBloc(),
      child: BlocBuilder<ThemeBloc, ThemeState>(
        builder: (context, state) {
          return MaterialApp(
            theme: ThemeData(
              brightness: Brightness.light,
              useMaterial3: true,
              colorSchemeSeed: Colors.green,
              scaffoldBackgroundColor: Colors.grey[50],
              textTheme: const TextTheme(
                titleLarge:
                    TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                titleMedium:
                    TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                bodyMedium: TextStyle(color: CupertinoColors.secondaryLabel),
              ),
            ),
            darkTheme: ThemeData(
              brightness: Brightness.dark,
              useMaterial3: true,
              colorSchemeSeed: Colors.purple,
              scaffoldBackgroundColor: Colors.grey[850],
              textTheme: const TextTheme(
                titleLarge:
                    TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                titleMedium:
                    TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
            themeMode: state.isDarkTheme ? ThemeMode.dark : ThemeMode.light,
            localizationsDelegates: AppLocalizations.localizationsDelegates,
            supportedLocales: AppLocalizations.supportedLocales,
            debugShowCheckedModeBanner: false,
            home: const HomePage(),
          );
        },
      ),
    );
  }
}
