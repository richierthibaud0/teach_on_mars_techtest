import 'package:flutter/material.dart';
import 'package:teach_on_mars_techtest/l10n/l10n.dart';
import 'package:teach_on_mars_techtest/models/models.dart';

class PictureDetailPage extends StatelessWidget {
  const PictureDetailPage({
    super.key,
    required this.picturePost,
  });

  final PicturePost picturePost;

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    return Scaffold(
      appBar: AppBar(title: Text(l10n.pictureDetailPageAppBarTitle)),
      body: SingleChildScrollView(
        child: Column(
          children: [
            AspectRatio(
              aspectRatio: 16 / 11,
              child: Hero(
                tag: picturePost.id,
                child: Image.network(
                  picturePost.picture.url,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            const SizedBox(height: 16),
            Text(
              picturePost.title,
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(height: 4),
            Text(
              picturePost.author,
              style: Theme.of(context).textTheme.bodyMedium,
            ),
          ],
        ),
      ),
    );
  }
}
