import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teach_on_mars_techtest/presentation/presentation.dart';
import 'package:teach_on_mars_techtest/repos/repos.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc(this._feedRepository) : super(HomeLoadingState()) {
    on<LoadHomeEvent>((event, emit) async {
      emit(HomeLoadingState());
      try {
        final posts = await _feedRepository.getFeed();
        emit(HomeLoadedState(posts));
      } catch (e) {
        emit(HomeErrorState(e.toString()));
      }
    });
  }
  final FeedRepository _feedRepository;
}
