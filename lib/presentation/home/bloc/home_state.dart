import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:teach_on_mars_techtest/models/models.dart';

@immutable
abstract class HomeState extends Equatable {}

class HomeLoadingState extends HomeState {
  @override
  List<Object?> get props => [];
}

class HomeLoadedState extends HomeState {
  HomeLoadedState(this.posts);
  final List<IPost> posts;
  @override
  List<Object?> get props => [posts];
}

class HomeErrorState extends HomeState {
  HomeErrorState(this.error);
  final String error;
  @override
  List<Object?> get props => [error];
}
