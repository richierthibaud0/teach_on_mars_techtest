import 'package:flutter/material.dart';
import 'package:teach_on_mars_techtest/l10n/l10n.dart';
import 'package:teach_on_mars_techtest/models/models.dart';
import 'package:teach_on_mars_techtest/presentation/presentation.dart';

class ArticlePostWidget extends StatelessWidget {
  const ArticlePostWidget({
    super.key,
    required this.article,
  });

  final ArticlePost article;

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    return PostCard(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            article.title,
            style: Theme.of(context).textTheme.titleMedium,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          const SizedBox(height: 8),
          Text(article.excerpt),
          const SizedBox(height: 8),
          SizedBox(
            width: double.infinity,
            child: FilledButton(
              onPressed: () => Navigator.push(
                context,
                MaterialPageRoute<ArticleDetailPage>(
                  builder: (context) => ArticleDetailPage(article: article),
                ),
              ),
              child: Text(l10n.showMoreArticleButton),
            ),
          ),
        ],
      ),
    );
  }
}
