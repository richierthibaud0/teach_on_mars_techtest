import 'package:flutter/material.dart';
import 'package:teach_on_mars_techtest/models/models.dart';
import 'package:teach_on_mars_techtest/presentation/presentation.dart';

class PicturePostWidget extends StatelessWidget {
  const PicturePostWidget({
    super.key,
    required this.picturePost,
  });

  final PicturePost picturePost;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute<PictureDetailPage>(
          builder: (context) => PictureDetailPage(picturePost: picturePost),
        ),
      ),
      child: PostCard(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              picturePost.title,
              style: Theme.of(context).textTheme.titleMedium,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
            const SizedBox(height: 8),
            ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: AspectRatio(
                aspectRatio: 16 / 9,
                child: Hero(
                  tag: picturePost.id,
                  child: Image.network(
                    picturePost.picture.url,
                    fit: BoxFit.cover,
                    frameBuilder: (
                      _,
                      Widget child,
                      int? frame,
                      bool wasSynchronouslyLoaded,
                    ) {
                      if (wasSynchronouslyLoaded) {
                        return child;
                      }
                      return AnimatedOpacity(
                        opacity: frame == null ? 0 : 1,
                        duration: const Duration(seconds: 1),
                        curve: Curves.easeOut,
                        child: child,
                      );
                    },
                  ),
                ),
              ),
            ),
            const SizedBox(height: 8),
            Center(
              child: Text(picturePost.author),
            ),
          ],
        ),
      ),
    );
  }
}
