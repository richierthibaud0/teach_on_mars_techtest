import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teach_on_mars_techtest/app/app.dart';
import 'package:teach_on_mars_techtest/l10n/l10n.dart';
import 'package:teach_on_mars_techtest/models/models.dart';
import 'package:teach_on_mars_techtest/presentation/presentation.dart';
import 'package:teach_on_mars_techtest/repos/repos.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final bottomPadding = MediaQuery.of(context).padding.bottom;
    final l10n = context.l10n;
    return BlocProvider<HomeBloc>(
      create: (context) => HomeBloc(FeedRepository())..add(LoadHomeEvent()),
      child: Scaffold(
        appBar: AppBar(
          key: const Key('AppBar'),
          leading: IconButton(
            icon: const Icon(Icons.menu),
            onPressed: () {},
          ),
          actions: const [
            _ReloadApiWidget(),
            _ThemeSwitch(),
          ],
          title: Text(l10n.feedAppBarTitle),
        ),
        body: _FeedWidget(bottomPadding: bottomPadding),
      ),
    );
  }
}

class _FeedWidget extends StatelessWidget {
  const _FeedWidget({
    required this.bottomPadding,
  });

  final double bottomPadding;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocBuilder<HomeBloc, HomeState>(
        builder: (context, state) {
          if (state is HomeLoadingState) {
            return const CircularProgressIndicator.adaptive();
          } else if (state is HomeErrorState) {
            return Center(
              child: Text(
                state.error,
                textAlign: TextAlign.center,
              ),
            );
          } else if (state is HomeLoadedState) {
            final posts = state.posts;
            return Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: ListView.separated(
                  padding: EdgeInsets.only(
                    top: 24,
                    bottom: bottomPadding != 0 ? bottomPadding : 24,
                  ),
                  itemCount: posts.length,
                  separatorBuilder: (context, index) =>
                      const SizedBox(height: 16),
                  itemBuilder: (context, index) {
                    final element = posts[index];
                    switch (element.type) {
                      case 1:
                        return ArticlePostWidget(
                          key: Key(element.id),
                          article: element as ArticlePost,
                        );
                      case 2:
                        return PicturePostWidget(
                          key: Key(element.id),
                          picturePost: element as PicturePost,
                        );
                      default:
                        break;
                    }
                    return null;
                  },
                ),
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}

class _ReloadApiWidget extends StatelessWidget {
  const _ReloadApiWidget();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (context, state) {
        if (state is HomeLoadingState) {
          return const SizedBox.shrink();
        } else if (state is HomeErrorState || state is HomeLoadedState) {
          return IconButton(
            onPressed: () => context.read<HomeBloc>().add(LoadHomeEvent()),
            icon: const Icon(Icons.refresh),
          );
        }
        return Container();
      },
    );
  }
}

class _ThemeSwitch extends StatelessWidget {
  const _ThemeSwitch();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ThemeBloc, ThemeState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.only(right: 12),
          child: Switch(
            value: state.isDarkTheme,
            thumbIcon: MaterialStateProperty.resolveWith<Icon?>(
                (Set<MaterialState> states) {
              if (states.contains(MaterialState.selected)) {
                return const Icon(Icons.dark_mode);
              }
              return const Icon(Icons.light_mode);
            }),
            onChanged: (value) => context.read<ThemeBloc>().add(
                  ChangeTheme(),
                ),
          ),
        );
      },
    );
  }
}
